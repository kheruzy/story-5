# Generated by Django 2.2.6 on 2019-10-06 14:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='calendar',
            old_name='kategori',
            new_name='cat',
        ),
        migrations.RenameField(
            model_name='calendar',
            old_name='tempat',
            new_name='location',
        ),
        migrations.RenameField(
            model_name='calendar',
            old_name='nama',
            new_name='title',
        ),
    ]
