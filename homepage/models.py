from django.db import models

# Create your models here.
class Schedule(models.Model):

    # Fields
    title = models.CharField(max_length=50)
    location = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    category = models.CharField(max_length=100) 

    def __str__(self):
        return self.title