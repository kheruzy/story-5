from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('contact/', views.contact, name='contact'),
    path('gallery/', views.gallery, name='gallery'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create/', views.create_schedule, name='create_schedule'),
    path('schedule/delete', views.delete_schedule, name='delete_schedule'),
]