from django.shortcuts import render, redirect
from .models import Schedule
from . import forms


# Create your views here.
def homepage(request):
    return render(request, 'homepage.html')

def contact(request):
    return render(request, 'contact.html')

def gallery(request):
    return render(request, 'gallery.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html',{'schedules':schedules})

def create_schedule(request):
    if request.method == 'POST':
        form = forms.CreateSchedule(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:schedule')
    else:
        form = forms.CreateSchedule()
    return render(request, 'create_schedule.html',{'form': form})

def delete_schedule(request):
    if request.method == "POST":
        id = request.POST['id']
        Schedule.objects.get(id=id).delete()
    return redirect('homepage:schedule')
